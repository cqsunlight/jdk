FROM centos:7
MAINTAINER hammer "617731010@qq.com"
WORKDIR /usr/java
ADD ./jdkVersion/jdk-8u301-linux-x64.tar.gz /usr/java
 
ENV JAVA_HOME=/usr/java/jdk1.8.0_301
ENV CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
ENV PATH=$JAVA_HOME/bin:$PATH
